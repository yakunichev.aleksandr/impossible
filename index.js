const Koa = require('koa');
const Sound = require('aplay');
const app = new Koa();
const p = require("path");

const PATH = './sounds/';

const SOUNDS = {
  1: ['1-1.wav', '1-2.wav', '1-3.wav', '1-4.wav', '1-5.wav', '1-6.wav'],
  2: ['2-1.wav', '2-2.wav', '2-3.wav', '2-4.wav', '2-5.wav'],
  3: ['3-1.wav', '3-2.wav', '3-3.wav'],
  4: ['4-1.wav', '4-2.wav', '4-3.wav', '4-4.wav', '4-5.wav', '4-6.wav'],
  5: ['5-1.wav', '5-2.wav'],
};

app.use(async (ctx, next) => {
  const path = Number(ctx.request.path.replace('/', '')) || 1;

  const sounds = SOUNDS[path];
  const sound = sounds[Math.round(Math.random() * (sounds.length - 1))];

  new Sound().play(p.resolve(__dirname, PATH, sound));

  ctx.body = '';
});

app.listen(80);
